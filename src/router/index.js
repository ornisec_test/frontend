import { createRouter, createWebHashHistory } from 'vue-router'
import HomeView from '../views/HomeView.vue'
import EquipmentList from "@/views/equipments/EquipmentList";
import FirmwareList from "@/views/firmwares/FirmwareList";
import FirmwareDetail from "@/views/firmwares/FirmwareDetail";

const routes = [
  {
    path: '/',
    name: 'home',
    component: HomeView
  },
  {
    path: '/equipments',
    name: 'equipments',
    component: EquipmentList
  },
  {
    path: '/firmwares',
    name: "firmwares",
    component: FirmwareList
  },
  {
    path: '/firmwares/:id',
    name: "firmwareDetail",
    component: FirmwareDetail

  }

]

const router = createRouter({
  history: createWebHashHistory(),
  routes
})

export default router
